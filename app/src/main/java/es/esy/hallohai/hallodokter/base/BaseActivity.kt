package es.esy.hallohai.hallodokter.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 09.13.
 */
open class BaseActivity : AppCompatActivity() {

    lateinit var mActiviy: AppCompatActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActiviy = this
    }
}