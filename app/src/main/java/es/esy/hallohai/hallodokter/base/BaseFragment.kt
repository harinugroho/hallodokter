package es.esy.hallohai.hallodokter.base

import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 09.13.
 */
open class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}