package es.esy.hallohai.hallodokter.data.model

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 09.33.
 */
data class Article (
        val imageUrl: String = "",
        val title: String = "",
        val contentResume: String = "",
        val content: String = "",
        val timestamp: Long = 0
){
    val articleId: String = ""
    val contact: List<Doctor>? = null
}