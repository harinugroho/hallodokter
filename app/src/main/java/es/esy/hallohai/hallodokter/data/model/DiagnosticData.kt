package es.esy.hallohai.hallodokter.data.model

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 09.41.
 */
data class DiagnosticData (
        val disease: String = "",
        val dizzy: String = "",
        val nausea: String = "",
        val temperature: Int = 0,
        val age: Int = 0
)