package es.esy.hallohai.hallodokter.data.model

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 10.17.
 */
data class Doctor (
        val name: String = "",
        val phoneNumber: String = ""
)