package es.esy.hallohai.hallodokter.data.source

import es.esy.hallohai.hallodokter.data.source.local.HalloDokterLocalDatasource

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 09.29.
 */
class HalloDokterRepository(
        val halloDokterRemoteDatasource: HalloDokterDataSource,
        val halloDokterLocalDatasource: HalloDokterDataSource
): HalloDokterDataSource {
    override fun getAllArticle() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {

        private var INSTANCE: HalloDokterRepository? = null

        @JvmStatic
        fun getInstance(mMobilePayslipRemoteDataSource: HalloDokterDataSource, mMobilePayslipLocalDataSource: HalloDokterDataSource) =
                INSTANCE ?: synchronized(HalloDokterRepository::class.java) {
                    INSTANCE ?: HalloDokterRepository(mMobilePayslipRemoteDataSource, mMobilePayslipLocalDataSource)
                            .also { INSTANCE = it }
                }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}