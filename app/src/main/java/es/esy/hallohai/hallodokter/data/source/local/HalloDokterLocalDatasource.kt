package es.esy.hallohai.hallodokter.data.source.local

import android.content.SharedPreferences
import android.support.annotation.VisibleForTesting
import es.esy.hallohai.hallodokter.data.source.HalloDokterDataSource
import es.esy.hallohai.hallodokter.util.AppExecutors

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 10.57.
 */
class HalloDokterLocalDatasource private constructor(
        val appExecutors: AppExecutors,
        val preferences: SharedPreferences
): HalloDokterDataSource {
    override fun getAllArticle() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        private var INSTANCE: HalloDokterLocalDatasource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, preferences: SharedPreferences): HalloDokterLocalDatasource {
            if (INSTANCE == null) {
                synchronized(HalloDokterLocalDatasource::javaClass) {
                    INSTANCE = HalloDokterLocalDatasource(appExecutors, preferences)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}