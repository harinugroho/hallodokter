package es.esy.hallohai.hallodokter.mvvm.article

import android.os.Bundle
import es.esy.hallohai.hallodokter.R
import es.esy.hallohai.hallodokter.base.BaseActivity
import es.esy.hallohai.hallodokter.util.obtainViewModel
import es.esy.hallohai.hallodokter.util.replaceFragmentInActivity

/**
 * TODO: Masukkan JavaDok
 */
class ArticleActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)

        setupFragment()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_article_content)
        ArticleFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_article_content)
        }
    }

    fun obtainViewModel(): ArticleViewModel = obtainViewModel(ArticleViewModel::class.java)
}
