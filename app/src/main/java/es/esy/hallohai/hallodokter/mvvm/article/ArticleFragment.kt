package es.esy.hallohai.hallodokter.mvvm.article


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import es.esy.hallohai.hallodokter.base.BaseFragment
import es.esy.hallohai.hallodokter.databinding.FragmentArticleBinding
import kotlinx.android.synthetic.main.fragment_article.*

/**
 * TODO: Masukkan JavaDok
 */
class ArticleFragment : BaseFragment(), OnMapReadyCallback {

    private lateinit var viewDataBinding: FragmentArticleBinding
    private lateinit var mMap: GoogleMap

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewDataBinding = FragmentArticleBinding.inflate(inflater, container, false).apply {
            viewmodel = (activity as ArticleActivity).obtainViewModel()
        }

        val mapView = viewDataBinding.map
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)

        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewDataBinding.viewmodel?.start()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setMarker(LatLng(-6.924362, 107.614100))
    }

    private fun setMarker(latLng: LatLng) {
        mMap.addMarker(MarkerOptions()
                .position(latLng))
        val cameraPosition = CameraPosition.Builder()
                .target(latLng)
                .zoom(17f)
                .build()
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    companion object {
        fun newInstance(): ArticleFragment {
            val fragment = ArticleFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
