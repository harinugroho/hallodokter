package es.esy.hallohai.hallodokter.mvvm.article

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import es.esy.hallohai.hallodokter.data.model.Article
import es.esy.hallohai.hallodokter.data.source.HalloDokterRepository

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 14.52.
 */
class ArticleViewModel (
        private val context: Application,
        private val halloDokterRepository: HalloDokterRepository
) : AndroidViewModel(context){

    var imageUrl = ObservableField<String>()
    var title = ObservableField<String>()
    var contentResume = ObservableField<String>()
    var content = ObservableField<String>()
    var timestamp = ObservableField<Long>()

    fun start(){
        imageUrl.set("url")
        title.set("Lorem ipsum")
        contentResume.set("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id imperdiet ligula. Etiam rhoncus fermentum commodo.")
        content.set("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id imperdiet ligula. Etiam rhoncus fermentum commodo. Vestibulum iaculis ante lectus, sed egestas est tincidunt vel. Nunc mollis, nisl ac scelerisque dignissim, diam felis dapibus lorem, et sodales justo urna vitae orci. Nulla eget leo ligula. Nunc tempus laoreet ante. Quisque gravida magna porttitor est gravida, at consequat sapien semper. Nunc condimentum nulla eu interdum viverra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur molestie tincidunt neque a fermentum. Sed ullamcorper urna sit amet orci porttitor placerat. Ut vulputate, tortor eget molestie congue, orci tellus volutpat justo, eget tincidunt ipsum metus et neque. Nullam sit amet magna elementum, tincidunt lectus pellentesque, bibendum eros. In et lobortis dui. Phasellus eget est at nisl tincidunt tincidunt. Aenean in lectus justo. Cras sed quam tempor, pulvinar ligula vel, venenatis nulla. Donec rutrum semper varius. Morbi convallis consectetur ligula ut eleifend. Vivamus fermentum libero id facilisis hendrerit. Vestibulum varius dolor ut convallis ultricies. Nulla porttitor orci vel mattis venenatis. Nam vitae dictum ligula. Pellentesque a tincidunt tortor, ut maximus turpis. Curabitur lacinia lectus sodales, porttitor nunc vel, tincidunt velit. Praesent eget semper neque. Sed eget quam diam. Aenean commodo semper accumsan. Proin et nulla efficitur, elementum odio sed, ullamcorper elit. Phasellus at tincidunt purus. In hac habitasse platea dictumst. Nulla efficitur purus in libero gravida, sit amet tempor libero rutrum. Nam facilisis ullamcorper metus a placerat. Duis fringilla faucibus nibh id volutpat. Vestibulum varius sollicitudin sem, at dapibus est pharetra et. Mauris eleifend nisl quis tempus eleifend. Maecenas molestie consectetur elit, quis aliquam diam ullamcorper vel. Suspendisse quis lectus vel odio mattis malesuada a sed nibh. Vivamus convallis sapien a vulputate eleifend. Nunc nec consectetur dui, vel pellentesque erat. Ut sit amet varius eros. Nullam quis fermentum mauris. Pellentesque cursus scelerisque condimentum. Nam efficitur nulla urna, non aliquet nisi faucibus non. Phasellus auctor vehicula urna ut tempor. Mauris maximus, quam id venenatis dictum, massa justo gravida mauris, in rhoncus neque sem ut mauris. Pellentesque congue lorem nibh, sit amet posuere metus luctus eu. Donec enim lorem, ultrices et congue non, dapibus nec urna. Nam viverra auctor lacus. Integer sit amet felis in nisi blandit interdum. Vestibulum nec diam nec ipsum faucibus blandit. Donec tincidunt eros venenatis, blandit turpis et, finibus turpis. Cras quis lorem neque. Nullam vitae dolor id diam semper sagittis a vitae dui. Aliquam est mauris, vehicula eget pulvinar id, convallis non arcu. Nulla facilisi. Praesent efficitur at quam eget suscipit. Maecenas malesuada ex id rhoncus finibus.")
        timestamp.set(1522418809)
    }

}