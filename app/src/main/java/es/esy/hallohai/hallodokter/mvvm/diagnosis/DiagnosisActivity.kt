package es.esy.hallohai.hallodokter.mvvm.diagnosis

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import es.esy.hallohai.hallodokter.R
import es.esy.hallohai.hallodokter.base.BaseActivity
import es.esy.hallohai.hallodokter.mvvm.diagnosisresult.DiagnosisResultActivity
import es.esy.hallohai.hallodokter.util.obtainViewModel
import es.esy.hallohai.hallodokter.util.replaceFragmentInActivity

/**
 * TODO: Masukkan JavaDok
 */
class DiagnosisActivity : BaseActivity(), DiagnosisUserActionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diagnosis)

        setupFragment()
        subscribeEvent()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_diagnosis_content)
        DiagnosisFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_diagnosis_content)
        }
    }

    private fun subscribeEvent() {
        obtainViewModel().apply {
            diagnosisEvent.observe(this@DiagnosisActivity, Observer {
                diagnosis()
            })
        }
    }

    override fun diagnosis() {
        val intent = Intent(this@DiagnosisActivity, DiagnosisResultActivity::class.java)
        startActivity(intent)
    }

    fun obtainViewModel(): DiagnosisViewModel = obtainViewModel(DiagnosisViewModel::class.java)
}
