package es.esy.hallohai.hallodokter.mvvm.diagnosis


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import es.esy.hallohai.hallodokter.base.BaseFragment
import es.esy.hallohai.hallodokter.databinding.FragmentDiagnosisBinding

/**
 * TODO: Masukkan JavaDok
 */
class DiagnosisFragment : BaseFragment() {

    private lateinit var viewDataBinding: FragmentDiagnosisBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewDataBinding = FragmentDiagnosisBinding.inflate(inflater, container, false).apply {
            viewmodel = (activity as DiagnosisActivity).obtainViewModel()
        }

        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupDiagnosisButton()
    }

    private fun setupDiagnosisButton() {
        viewDataBinding.listener = object : DiagnosisUserActionListener{
            override fun diagnosis() {
                viewDataBinding.viewmodel?.processForDiagnosis()
            }

        }
    }

    companion object {
        fun newInstance(): DiagnosisFragment {
            val fragment = DiagnosisFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
