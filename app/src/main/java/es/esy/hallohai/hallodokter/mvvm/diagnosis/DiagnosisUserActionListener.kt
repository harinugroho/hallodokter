package es.esy.hallohai.hallodokter.mvvm.diagnosis

import es.esy.hallohai.hallodokter.data.model.DiagnosticData

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 12.58.
 */
interface DiagnosisUserActionListener {

    fun diagnosis()

}