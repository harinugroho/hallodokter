package es.esy.hallohai.hallodokter.mvvm.diagnosis

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import es.esy.hallohai.hallodokter.data.model.DiagnosticData
import es.esy.hallohai.hallodokter.data.source.HalloDokterRepository
import es.esy.hallohai.hallodokter.util.SingleLiveEvent

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 12.57.
 */
class DiagnosisViewModel(
        context: Application,
        private val halloDokterRepository: HalloDokterRepository
) : AndroidViewModel(context){

    internal val diagnosisEvent = SingleLiveEvent<DiagnosticData>()

    fun processForDiagnosis(){
        diagnosisEvent.call()
    }

}