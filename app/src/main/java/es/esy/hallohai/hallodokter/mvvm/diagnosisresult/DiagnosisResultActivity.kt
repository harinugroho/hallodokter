package es.esy.hallohai.hallodokter.mvvm.diagnosisresult

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import es.esy.hallohai.hallodokter.R
import es.esy.hallohai.hallodokter.base.BaseActivity
import es.esy.hallohai.hallodokter.data.model.Article
import es.esy.hallohai.hallodokter.mvvm.article.ArticleActivity
import es.esy.hallohai.hallodokter.util.obtainViewModel
import es.esy.hallohai.hallodokter.util.replaceFragmentInActivity

/**
 * TODO: Masukkan JavaDok
 */
class DiagnosisResultActivity : BaseActivity(), ResultItemUserActionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diagnosis_result)

        setupFragment()
        subscribeEvent()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_diagnosis_result_content)
        DiagnosisResultFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_diagnosis_result_content)
        }
    }

    private fun subscribeEvent(){
        obtainViewModel().apply {
            readArticle.observe(this@DiagnosisResultActivity, Observer { article ->
                if (article != null) {
                    onResultClick(article)
                }
            })
        }
    }

    override fun onResultClick(article: Article) {
        val intent = Intent(this@DiagnosisResultActivity, ArticleActivity::class.java)
        startActivity(intent)
    }

    fun obtainViewModel(): DiagnosisResultViewModel = obtainViewModel(DiagnosisResultViewModel::class.java)
}
