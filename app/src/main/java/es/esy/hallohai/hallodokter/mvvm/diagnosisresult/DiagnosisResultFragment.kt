package es.esy.hallohai.hallodokter.mvvm.diagnosisresult


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import es.esy.hallohai.hallodokter.base.BaseFragment
import es.esy.hallohai.hallodokter.databinding.FragmentDiagnosisResultBinding

/**
 * TODO: Masukkan JavaDok
 */
class DiagnosisResultFragment : BaseFragment() {

    private lateinit var viewDataBinding: FragmentDiagnosisResultBinding
    private lateinit var mAdapter: ResultAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentDiagnosisResultBinding.inflate(inflater, container, false).apply {
            viewmodel = (activity as DiagnosisResultActivity).obtainViewModel()
        }

        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loadData()
        setupResults()
    }

    private fun loadData(){
        viewDataBinding.viewmodel?.start()
    }

    private fun setupResults() {
        val mViewModel = viewDataBinding.viewmodel

        if (mViewModel != null) {
            mAdapter = ResultAdapter(mViewModel.articleList, mViewModel)

            viewDataBinding.listArticle.adapter = mAdapter

            val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
            viewDataBinding.listArticle.layoutManager = layoutManager
        }
    }

    companion object {
        fun newInstance(): DiagnosisResultFragment {
            val fragment = DiagnosisResultFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
