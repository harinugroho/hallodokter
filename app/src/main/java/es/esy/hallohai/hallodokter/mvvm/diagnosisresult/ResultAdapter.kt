package es.esy.hallohai.hallodokter.mvvm.diagnosisresult

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import es.esy.hallohai.hallodokter.R
import es.esy.hallohai.hallodokter.data.model.Article
import es.esy.hallohai.hallodokter.databinding.ItemSmallArticleBinding

/**
 * Created with love by Hari Nugroho on 31/03/2018 at 13.59.
 */
class ResultAdapter (private var dataset: List<Article>, private var diagnosisResultViewModel: DiagnosisResultViewModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val record = dataset[position]

        val mUserActionListener = object : ResultItemUserActionListener {

            override fun onResultClick(article: Article) {
                diagnosisResultViewModel.readArticle.value = article
            }
        }

        (holder as RecordItemHolder).bindItem(record, mUserActionListener)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    fun replaceData(articles : List<Article>) {
        setList(articles)
    }

    private fun setList(articles : List<Article>) {
        this.dataset = articles
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: ItemSmallArticleBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.item_small_article, parent, false)

        return RecordItemHolder(binding)
    }

    class RecordItemHolder(itemView: ItemSmallArticleBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val binding: ItemSmallArticleBinding = itemView

        fun bindItem(article: Article, userActionListener: ResultItemUserActionListener) {
            binding.viewmodel = article
            binding.listener = userActionListener
            binding.executePendingBindings()
        }
    }
}