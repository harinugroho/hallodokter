package es.esy.hallohai.hallodokter.mvvm.diagnosisresult

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import es.esy.hallohai.hallodokter.data.model.Article

/**
 * Created with love by Hari Nugroho on 31/03/2018 at 13.58.
 */
object ResultBindings {
    @BindingAdapter("app:resultList")
    @JvmStatic
    fun setResultList(recyclerView: RecyclerView, articles: List<Article>) {

        with(recyclerView.adapter as ResultAdapter) {
            replaceData(articles)
        }
    }
}