package es.esy.hallohai.hallodokter.mvvm.diagnosisresult

import es.esy.hallohai.hallodokter.data.model.Article

/**
 * Created with love by Hari Nugroho on 31/03/2018 at 13.58.
 */
interface ResultItemUserActionListener {
    fun onResultClick(article: Article)
}