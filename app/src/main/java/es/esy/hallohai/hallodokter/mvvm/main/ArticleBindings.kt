package es.esy.hallohai.hallodokter.mvvm.main

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import es.esy.hallohai.hallodokter.data.model.Article

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 21.34.
 */
object ArticleBindings {
    @BindingAdapter("app:articleList")
    @JvmStatic
    fun setArticleList(recyclerView: RecyclerView, articles: List<Article>) {

        with(recyclerView.adapter as LargeArticleAdapter) {
            replaceData(articles)
        }
    }
}