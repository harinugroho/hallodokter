package es.esy.hallohai.hallodokter.mvvm.main

import es.esy.hallohai.hallodokter.data.model.Article

/**
 * Created with love by Hari Nugroho on 31/03/2018 at 10.02.
 */
interface ArticleItemUserActionListener {
    fun onAticleClick(article: Article)
}