package es.esy.hallohai.hallodokter.mvvm.main

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import es.esy.hallohai.hallodokter.R
import es.esy.hallohai.hallodokter.data.model.Article
import es.esy.hallohai.hallodokter.databinding.ItemLargeArticleBinding

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 20.43.
 */
class LargeArticleAdapter(private var dataset: List<Article>, private var mainViewModel: MainViewModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val record = dataset[position]

        val mUserActionListener = object : ArticleItemUserActionListener {

            override fun onAticleClick(article: Article) {
                mainViewModel.readArticle.value = article
            }
        }

        (holder as RecordItemHolder).bindItem(record, mUserActionListener)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    fun replaceData(articles : List<Article>) {
        setList(articles)
    }

    private fun setList(articles : List<Article>) {
        this.dataset = articles
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: ItemLargeArticleBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.item_large_article, parent, false)

        return RecordItemHolder(binding)
    }

    class RecordItemHolder(itemView: ItemLargeArticleBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val binding: ItemLargeArticleBinding = itemView

        fun bindItem(article: Article, userActionListener: ArticleItemUserActionListener) {
            binding.viewmodel = article
            binding.listener = userActionListener
            binding.executePendingBindings()
        }
    }
}