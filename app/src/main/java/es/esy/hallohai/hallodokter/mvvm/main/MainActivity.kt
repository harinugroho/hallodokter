package es.esy.hallohai.hallodokter.mvvm.main

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import es.esy.hallohai.hallodokter.R
import es.esy.hallohai.hallodokter.base.BaseActivity
import es.esy.hallohai.hallodokter.data.model.Article
import es.esy.hallohai.hallodokter.mvvm.article.ArticleActivity
import es.esy.hallohai.hallodokter.mvvm.diagnosis.DiagnosisActivity
import es.esy.hallohai.hallodokter.util.helper.Arguments
import es.esy.hallohai.hallodokter.util.obtainViewModel
import es.esy.hallohai.hallodokter.util.replaceFragmentInActivity

/**
 * TODO: Masukkan JavaDok
 */
class MainActivity : BaseActivity(), MainUserActionListener, ArticleItemUserActionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupFragment()
        subscribeEvent()
    }

    private fun subscribeEvent(){
        obtainViewModel().apply {
            startDiagnosis.observe(this@MainActivity, Observer {
                onStartDiagnosisClicked()
            })
        }
        obtainViewModel().apply {
            readArticle.observe(this@MainActivity, Observer { article ->
                if (article != null) {
                    onAticleClick(article)
                }
            })
        }
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        MainFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    override fun onAticleClick(article: Article) {
        val intent = Intent(this@MainActivity, ArticleActivity::class.java)
        intent.putExtra(Arguments.ARG_ARTICLE_ID, article.articleId)
        startActivity(intent)
    }

    override fun onStartDiagnosisClicked() {
        val intent = Intent(this@MainActivity, DiagnosisActivity::class.java)
        startActivity(intent)
    }

    fun obtainViewModel(): MainViewModel = obtainViewModel(MainViewModel::class.java)
}
