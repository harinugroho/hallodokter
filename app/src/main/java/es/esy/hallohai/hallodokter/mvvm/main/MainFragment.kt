package es.esy.hallohai.hallodokter.mvvm.main


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import es.esy.hallohai.hallodokter.base.BaseFragment
import es.esy.hallohai.hallodokter.databinding.FragmentMainBinding


/**
 * TODO: Masukkan JavaDok
 */
class   MainFragment : BaseFragment() {

    private lateinit var viewDataBinding: FragmentMainBinding
    private lateinit var mAdapter: LargeArticleAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewDataBinding = FragmentMainBinding.inflate(inflater, container, false).apply {
            viewmodel = (activity as MainActivity).obtainViewModel()
        }

        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loadData()
        setupArticles()
        setupStartDiagnosisButton()
    }

    private fun loadData(){
        viewDataBinding.viewmodel?.start()
    }

    private fun setupArticles() {
        val mViewModel = viewDataBinding.viewmodel

        if (mViewModel != null) {
            mAdapter = LargeArticleAdapter(mViewModel.articleList, mViewModel)

            viewDataBinding.listArticle.adapter = mAdapter

            val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
            viewDataBinding.listArticle.layoutManager = layoutManager
        }
    }

    private fun setupStartDiagnosisButton() {
        viewDataBinding.listener = object : MainUserActionListener{
            override fun onStartDiagnosisClicked() {
                viewDataBinding.viewmodel?.startDiagnosis!!.call()
            }

        }
    }

    companion object {
        fun newInstance(): MainFragment {
            val fragment = MainFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

}// Required empty public constructor
