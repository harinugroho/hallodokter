package es.esy.hallohai.hallodokter.mvvm.main

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 12.53.
 */
interface MainUserActionListener {
    fun onStartDiagnosisClicked()
}