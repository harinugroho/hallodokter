package es.esy.hallohai.hallodokter.mvvm.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import es.esy.hallohai.hallodokter.data.model.Article
import es.esy.hallohai.hallodokter.data.source.HalloDokterRepository
import es.esy.hallohai.hallodokter.util.SingleLiveEvent

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 11.42.
 */
class MainViewModel(
        context: Application,
        private val halloDokterRepository: HalloDokterRepository
) : AndroidViewModel(context){

    internal val startDiagnosis = SingleLiveEvent<Void>()
    internal val readArticle = SingleLiveEvent<Article>()

    val articleList: ObservableList<Article> = ObservableArrayList()

    fun start() {
        getListAllArticle()
    }

    private fun getListAllArticle() {
        val mutableArticles: MutableList<Article> = mutableListOf()
        mutableArticles.add(Article("", "Title Article 1", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        mutableArticles.add(Article("", "Title Article 2", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        mutableArticles.add(Article("", "Title Article 3", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        mutableArticles.add(Article("", "Title Article 4", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        mutableArticles.add(Article("", "Title Article 5", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        mutableArticles.add(Article("", "Title Article 6", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        mutableArticles.add(Article("", "Title Article 7", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        mutableArticles.add(Article("", "Title Article 8", "Resume Of content, Thisis resum of long article that wis save in long article", "This is long contet the all deskription of all atricle without many content in here so it is sample of content", 1522418809))
        val articles: List<Article> = mutableArticles

        with(articleList) {
            clear()
            addAll(articles)
        }
    }
}