package es.esy.hallohai.hallodokter.util

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 11.09.
 */
class DiskIOThreadExecutor : Executor {

    private val diskIO = Executors.newSingleThreadExecutor()

    override fun execute(command: Runnable) { diskIO.execute(command) }
}