package es.esy.hallohai.hallodokter.util

import android.databinding.BindingAdapter
import android.os.Build
import android.support.annotation.RequiresApi
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created with love by Hari Nugroho on 22/04/2018 at 20.57.
 */
object ImageLoadBindings {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @BindingAdapter("app:imageLoad")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: String?) {
        if (imageUrl != "" && imageUrl != null) {
            Picasso.with(view.context)
                    .load(imageUrl)
                    .into(view)
        }
    }
}