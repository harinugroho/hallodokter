package es.esy.hallohai.hallodokter.util

import android.content.Context
import android.preference.PreferenceManager
import es.esy.hallohai.hallodokter.data.source.HalloDokterRepository
import es.esy.hallohai.hallodokter.data.source.local.HalloDokterLocalDatasource
import es.esy.hallohai.hallodokter.data.source.remote.HalloDokterRemoteDatasource

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 11.23.
 */
object Injection {
    fun provideLiliRepository(context: Context): HalloDokterRepository {
        return HalloDokterRepository.getInstance(
                HalloDokterRemoteDatasource,
                HalloDokterLocalDatasource.getInstance(AppExecutors(), PreferenceManager.getDefaultSharedPreferences(context))
        )
    }
}