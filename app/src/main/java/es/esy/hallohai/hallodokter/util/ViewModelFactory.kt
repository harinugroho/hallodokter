package es.esy.hallohai.hallodokter.util

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import es.esy.hallohai.hallodokter.data.source.HalloDokterRepository
import es.esy.hallohai.hallodokter.mvvm.article.ArticleViewModel
import es.esy.hallohai.hallodokter.mvvm.diagnosis.DiagnosisViewModel
import es.esy.hallohai.hallodokter.mvvm.diagnosisresult.DiagnosisResultViewModel
import es.esy.hallohai.hallodokter.mvvm.main.MainViewModel

/**
 * Created with love by Hari Nugroho on 30/03/2018 at 11.04.
 */
class ViewModelFactory private constructor(
        private val mApplication: Application,
        private val mHalloDokterRepository: HalloDokterRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(MainViewModel::class.java) ->
                        MainViewModel(mApplication, mHalloDokterRepository)
                    isAssignableFrom(DiagnosisViewModel::class.java) ->
                        DiagnosisViewModel(mApplication, mHalloDokterRepository)
                    isAssignableFrom(DiagnosisResultViewModel::class.java) ->
                        DiagnosisResultViewModel(mApplication, mHalloDokterRepository)
                    isAssignableFrom(ArticleViewModel::class.java) ->
                        ArticleViewModel(mApplication, mHalloDokterRepository)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance(mApplication: Application) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(mApplication,
                            Injection.provideLiliRepository(mApplication.applicationContext))
                            .also { INSTANCE = it }
                }
    }
}